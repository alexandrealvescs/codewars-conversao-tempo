# Topicos

- Entender o problema
- Pensar em uma forma de resolve-lo separando em partes
- Codar de forma bruta
- Observar partes que podem ser otimizadas
- Procurar alternativas para otimizar e fazer a refatoração  