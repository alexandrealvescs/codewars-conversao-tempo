def format_duration(seconds):
    if seconds == 0:
        return 'now'

    tempo = [0, 0, 0, 0, 0]
    string_tempo = ['year', 'day', 'hour', 'minute', 'second']
    tempo_em_segundo = [31536000, 86400, 3600, 60, 1]
    
    # Loop para calcular o tempo
    for indice, i in enumerate(tempo_em_segundo):
        inteiro_e_resto = divmod(seconds, i)

        tempo[indice] = inteiro_e_resto[0]
        seconds = inteiro_e_resto[1]

        if inteiro_e_resto[1] == 0:
            break
    
    response = []
    
    for indice, valor_temporal in enumerate(tempo):
        if valor_temporal != 0:
            response.append(f"{tempo[indice]} {string_tempo[indice] + 's' if tempo[indice] > 1 else string_tempo[indice]}")

    res = ', '.join(response)[::-1].replace(',', 'dna ', 1)

    return res[::-1]
    
# print(format_duration(312452))
# format_duration(161)
# format_duration(120)
# print(divmod(60,15)) # primeiro indice é valor inteiro, segundo indice valor do resto

# divisao_inteira = 30 // 1
# resto_divisao = 60 % 19

# print(divisao_inteira)
# print(resto_divisao)
print(divmod(60, 15))