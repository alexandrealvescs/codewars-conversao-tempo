def format_duration(seconds):
    if seconds == 0:
        return 'now'

    # Calculando segundos
    elif seconds < 60:
        string_sec = 'seconds' if seconds > 1 else 'second'

        return f'{seconds} {string_sec}'

    # Calculando minutos
    elif seconds >= 60 and seconds < 3600: # 3600 segundos equivale a 1 hora
        min = seconds // 60
        sec = seconds % 60

        string_min = 'minutes' if min > 1 else 'minute'
        string_sec = 'seconds' if sec > 1 else 'second'

        if sec == 0:
            return f'{min} {string_min}'

        return f'{min} {string_min} and {sec} {string_sec}'

    # Calculando horas
    elif seconds >= 3600 and seconds < 86400: # 86400 segundos equivale a 1 dia | 86400/3600 = 24horas
        hour = seconds // 3600
        calc = seconds % 3600
        min = calc // 60
        sec = calc % 60

        string_hour = 'hours' if hour > 1 else 'hour'
        string_min = 'minutes' if min > 1 else 'minute'
        string_sec = 'seconds' if sec > 1 else 'second'
        
        if calc == 0:
            return f'{hour} {string_hour}'

        elif min == 0:
            return f'{hour} {string_hour} and {sec} {string_sec}'

        elif sec == 0:
            return f'{hour} {string_hour} and {min} {string_min}'

        return f'{hour} {string_hour}, {min} {string_min} and {sec} {string_sec}'

    # Calculando dias
    elif seconds >= 86400 and seconds < 31536000: # 31536000 segundos equivale a 365 dias | 31536000/86400 = 365dias
        day = seconds // 86400
        calc1 = seconds % 86400
        hour = calc1 // 3600
        calc2 = calc1 % 3600
        min = calc2 // 60
        sec = calc2 % 60

        string_day = 'days' if day > 1 else 'day'
        string_hour = 'hours' if hour > 1 else 'hour'
        string_min = 'minutes' if min > 1 else 'minute'
        string_sec = 'seconds' if sec > 1 else 'second'

        if calc1 == 0:
            return f'{day} {string_day}'

        if calc2 == 0:
            return f'{day} {string_day} and {hour} {string_hour}'

        elif sec == 0:
            return f'{day} {string_day}, {hour} {string_hour} and {min} {string_min}'

        elif min == 0:
            return f'{day} {string_day}, {hour} {string_hour} and {sec} {string_sec}'

        elif hour == 0:
            return f'{day} {string_day}, {min} {string_min} and {sec} {string_sec}'

        return f'{day} {string_day}, {hour} {string_hour}, {min} {string_min} and {sec} {string_sec}'

    # Calculando anos
    else:
        year = seconds // 31536000
        calc1 = seconds % 31536000
        day = calc1 // 86400
        calc2 = calc1 % 86400
        hour = calc2 // 3600
        calc3 = calc2 % 3600
        min = calc3 // 60
        sec = calc3 % 60

        string_year = 'years' if year > 1 else 'year' 
        string_day = 'days' if day > 1 else 'day'
        string_hour = 'hours' if hour > 1 else 'hour'
        string_min = 'minutes' if min > 1 else 'minute'
        string_sec = 'seconds' if sec > 1 else 'second'

        if calc1 == 0:
            return f'{year} {string_year}'

        if calc2 == 0:
            return f'{year} {string_year} and {day} {string_day}'

        if calc3 == 0:
            return f'{year} {string_year}, {day} {string_day} and {hour} {string_hour}'

        elif sec == 0:
            return f'{year} {string_year}, {day} {string_day}, {hour} {string_hour} and {min} {string_min}'

        elif min == 0:
            return f'{year} {string_year}, {day} {string_day}, {hour} {string_hour} and {sec} {string_sec}'

        elif hour == 0:
            return f'{year} {string_year}, {day} {string_day}, {min} {string_min} and {sec} {string_sec}'

        return f'{year} {string_year}, {day} {string_day}, {hour} {string_hour}, {min} {string_min} and {sec} {string_sec}'

# divisao_inteira = 60 // 19
# resto_divisao = 60 % 19

# r = 19 * 3

# print(divisao_inteira, r)
# print(resto_divisao, 60 - r)
# print(format_duration(30))
